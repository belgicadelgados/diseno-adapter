﻿using System;
using System.Collections.Generic;
using System.Text;

namespace proyecto_exposicion
{
    public class MotorDiesel : motor
    {
        public override void Acelerar()
        {
            Console.WriteLine("Acelerado en el motor diesel..");
        }
        public override void Arrancar()
        {
            Console.WriteLine("Arrancando el motor disel..");
        }
        public override void CargarCombustible()
        {
            Console.WriteLine("Cargando conbustible a motor a diesel..");
        }
        public override void Detener()
        {
            Console.WriteLine("Deteniendo el motor a diesel..");
        }

    }
}
