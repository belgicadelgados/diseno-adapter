﻿using System;
using System.Collections.Generic;
using System.Text;

namespace proyecto_exposicion
{
    public abstract class motor
    {
        public abstract void Acelerar();
        public abstract void Arrancar();
        public abstract void Detener();
        public abstract void CargarCombustible();
    }
}
