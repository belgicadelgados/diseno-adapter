﻿using System;
using System.Collections.Generic;
using System.Text;

namespace proyecto_exposicion
{ 
    public class MotorNaftero: motor 
    {
        public override void Acelerar()
        {
          Console.WriteLine("Acelerando el motor naftero..");
        }
        public override void Arrancar()
        {
           Console.WriteLine("Arrancando el motor naftero..");
        }
        public override void CargarCombustible()
        {
            Console.WriteLine(" Cargando conbustible al motor naftero..");
        }
        public override void Detener()
        {
            Console.WriteLine("Deteniendo el motor naftero..");
        }
    }
}
