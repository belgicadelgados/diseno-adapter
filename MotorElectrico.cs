﻿using System;
using System.Collections.Generic;
using System.Text;

namespace proyecto_exposicion
{
    public class MotorElectrico
    {
        bool _Conectado;
        bool _Activo;
        bool _Moviendo;

        public MotorElectrico()
        {
        }

        public void Conectar()
        {
            if (_Conectado)
                Console.WriteLine("Imposible conectar un motor electrico ya conectado!");
            else
            {
                _Conectado = true;
                Console.WriteLine("Motor conectado!");
            }


        }

        public void Activar()
        {

            if (_Activo)
                Console.WriteLine("Imposible activar un motor no conectado");
            else
            {
                _Activo = true;
                Console.WriteLine("Motor activado!");
            }

        }

        public void Mover()
        {
            if (_Moviendo)
            {
                _Moviendo = true;
                Console.WriteLine("Moviendo vehiculo con motor eléctrico...");
            }
            else
            {
                Console.WriteLine("El motor deberá estar conectado y activo!");
            }

        }

        public void Parar()
        {

            if (_Moviendo)
            {
                _Moviendo = false;
                Console.WriteLine("Parando vehiculo con motor eléctrico...");
            }
            else
            {
                Console.WriteLine("Imposible parar un motor que no esté en movimiento!");
            }

        }

        public void Desconectar()
        {
            if (_Conectado)
            {

                Console.WriteLine("Motor desconectado...");
            }
            else
            {
                Console.WriteLine("Imposible desconectar un motor que no esté conectado!");
            }

        }

        public void Desactivar()
        {
            if (_Activo)
            {
                _Activo= false;
                Console.WriteLine("Motor desactivado...");
            }
            else
            {
                Console.WriteLine("Imposible desactivar un motor que no esté activo!");
            }
        }

        public void Enchufar()
        {
            if (_Activo)
            {
                _Activo = false;
                Console.WriteLine("Motor cargando las baterias!...");
            }
            else
            {
                Console.WriteLine("Imposible enchufar un motor activo!");
            }

        }

    }
}
